import { controls } from '../../constants/controls'


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let maxHealthFirstFigter=firstFighter.health
    let maxHealthSecondFigter=secondFighter.health
    let leftIndicator=document.getElementById("left-fighter-indicator")
    let rightIndicator=document.getElementById("right-fighter-indicator")
    
    let firstFighterCanAttack=true
    let secondFighterCanAttack=true
    let firstFighterIsDefense=false
    let secondFighterIsDefense=false
    let firstFighterCanSuperAttack=true
    let secondFighterCanSuperAttack=true
    
    function runTripleKeys(funcUp, ...codes) {
      let pressed = new Set()
      document.addEventListener('keydown', function(event) {
        pressed.add(event.code)
        for (let code of codes) { 
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();
        funcUp();
      });
      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code)
      });
    }
    function runOneKey(funcDown,funcUp, code) {
      document.addEventListener('keydown', function(event) {
        if(event.code==code){funcDown()}
      });
      document.addEventListener('keyup', function(event) {
        if(event.code==code){funcUp()}
      });
    }
    
    
    function attackDown(attacker, defender, position){
      let damage
      switch(position){
        
        case "left": 
          if (!firstFighterCanAttack) return
          damage = secondFighterIsDefense ? 0 : getDamage(attacker, defender)
          defender.health-=damage
          firstFighterCanAttack=false
          checkStatus()
          return

        case "right": 
          if (!secondFighterCanAttack) return
          damage = firstFighterIsDefense ? 0 : getDamage(attacker, defender)
          defender.health-=damage
          firstFighterCanAttack=false
          checkStatus()
          return
        default:
          return
      }
    };
    function attackUp(position){
      switch(position){

        case "left": 
        if(!firstFighterIsDefense) firstFighterCanAttack=true
        return

        case "right": 
        if(!secondFighterIsDefense) secondFighterCanAttack=true
        return

        default:
          return
      }
      
    };
    function defenseDown(position){
      switch(position){

        case "left": 
          firstFighterIsDefense=true
          firstFighterCanAttack=false
          return

        case "right": 
          secondFighterIsDefense=true
          secondFighterCanAttack=false
          return

        default:
          return
      }
      
    };

    function defenseUp(position){
      switch(position){

        case "left":
          firstFighterIsDefense=false
          firstFighterCanAttack=true
          return

        case "right": 
          secondFighterIsDefense=false
          secondFighterCanAttack=true
          return

        default:
          return
      }
      
    };

    function criticalHit(attacker, defender, position){
      switch(position){

        case "left": 
          if(firstFighterCanAttack&&firstFighterCanSuperAttack){
            defender.health-=attacker.attack*2
            firstFighterCanSuperAttack=false
            setTimeout(() => firstFighterCanSuperAttack=true, 10000)
            checkStatus()
          }; 
          return

        case "right": 
          if(secondFighterCanAttack&&secondFighterCanSuperAttack){
            defender.health-=attacker.attack*2
            secondFighterCanSuperAttack=false
            setTimeout(() => secondFighterCanSuperAttack=true, 10000)
            checkStatus()
          }; 
          return

        default:
          return
      }
      
    };


    runTripleKeys(() => {criticalHit(firstFighter, secondFighter, "left")}, ...controls.PlayerOneCriticalHitCombination)
    runTripleKeys(() => {criticalHit(secondFighter, firstFighter, "right")}, ...controls.PlayerTwoCriticalHitCombination)
    runOneKey(() => {attackDown(firstFighter, secondFighter, "left")},()=>{attackUp("left")}, controls.PlayerOneAttack)
    runOneKey(()=>{defenseDown("left")},()=>{defenseUp("left")}, controls.PlayerOneBlock)
    runOneKey(() => {attackDown(secondFighter, firstFighter, "right")},()=>{attackUp("right")}, controls.PlayerTwoAttack)
    runOneKey(()=>{defenseDown("right")},()=>{defenseUp("right")}, controls.PlayerTwoBlock)
    function checkStatus(){

      leftIndicator.style.width=`${firstFighter.health/maxHealthFirstFigter*100}%`
      rightIndicator.style.width=`${secondFighter.health/maxHealthSecondFigter*100}%`
      if (firstFighter.health<=0||secondFighter.health<=0){
        let winFighter=firstFighter.health>0?firstFighter:secondFighter
        resolve(winFighter)
      };
    }
   
  });
}

export function getDamage(attacker, defender) {
    let damage=getHitPower(attacker)-getBlockPower(defender)
    return damage>0 ? damage : 0
}

export function getHitPower(fighter) {
  let criticalHitChance=Math.random()+1
  return fighter.attack*criticalHitChance
}

export function getBlockPower(fighter) {
  let dodgeChance=Math.random()+1
  let blockPower=fighter.defense*dodgeChance
  return blockPower
}

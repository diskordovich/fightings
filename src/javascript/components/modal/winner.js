import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 
  const winFighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root`,
  });
  if (fighter) {
    const fName = document.createElement('h2')
    fName.innerText = fighter.name
    winFighterElement.append(fName);

    const fHealth = document.createElement('h4')
    fHealth.innerText = `Health: ${fighter.health}`
    winFighterElement.append(fHealth);

    const fAttack = document.createElement('h4');
    fAttack.innerText = `Attack: ${fighter.attack}`
    winFighterElement.append(fAttack);

    const fDefense = document.createElement('h4');
    fDefense.innerText = `Defense: ${fighter.defense}`
    winFighterElement.append(fDefense);

    const fImg = document.createElement('img');
    fImg.src = `${fighter.source}`;
    fImg.height = 300;
    fImg.width = 200;
    fImg.className = "fighter-preview___root";
    fImg.alt = "fighter";
    winFighterElement.append(fImg);
  }
  showModal({ title: `${fighter.name} Congratulations!`, bodyElement: winFighterElement, onClose: () => { window.location.reload(); } });
}
